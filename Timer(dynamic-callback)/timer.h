/*
 * Author:  Dustin Haring
 * Date:    June 07, 2018
 * Comment: This file contains the declarations and definitions for a
 *          timer class. This timer class creates a timer upon
 *          instantiation and only one timer per instantiation.
 * 
 * Modified By: n/a
 * Date:        n/a
 * Comment:     n/a
 */

/** How often in microseconds timer thread should 
 * check for a kill command as well as how often to
 * check if we have timed out. The stop function will 
 * check 4 times as often to see if thread has died. 
 * The larger this number, the less accurate the 
 * timer will be. If set to 1000 microseconds (1ms), 
 * then timer will be off by ~1ms max from desired
 * timer timeout time. */
#define threadDeathAndTimeout_CheckTime 1000 

#ifndef ThreadTimer_H
#define ThreadTimer_H

#include <time.h> //for nano sleeping
#include <thread>
#include <mutex>
#include <assert.h>

typedef std::chrono::high_resolution_clock sysClock;

/** *****DESCRIPTION of class Timer*****
 * Create an instance of the Timer class and call
 * setTime to set a time in ms and Start() to start 
 * the timer. The timer must have a valid callback 
 * function set as well as a valid instance pointer 
 * to the callback's owning class before Start() can 
 * be called. You must set a valid timeout time 
 * (time > 0) before starting a timer as well. 
 * 
 * All function calls are thread safe and 
 * may be called from a timer's callback if desired.
 *
 * The Stop() function may be called to stop an
 * active timer. Calling Stop() will result in the 
 * calling thread halted until timer thread is 
 * killed. The timer will be
 * stopped within 1.25ms of being called. 
 * 
 * Start() will start a timer. It returns true if
 * a timer was successfully started, false otherwise.
 * Any calls to Start() will return false and will not
 * start a timer if a timer is currently running or in
 * the proccess of stopping. To guarantee a timer
 * gets started, use while(timer->Start());. This will
 * force the calling thread to wait until the timer has 
 * timed out and is ready to start again.
 * 
 * setCallback(ptr) will set the function that the timer
 * will call upon timeout. A callback 
 * may be set at anytime even while the timer is running
 * and may be called from a timers callback (thread safe).
 * 
 * If a new callback, timer timeout time, or if AutoReset is 
 * changed, the timer will see those changes upon it's 
 * next reset. 
 * 
 * setTime(uint) sets the timer timeout time (how long a timer
 * times for before calling its callback. Use getTime()
 * to retrieve the currently set timeout time.
 * 
 * isActive() returns true if timer is running, false otherwise.
 * 
 * */

template <class T>
class Timer
{
  public:
    typedef void (T::*CallbackFunct)();
    Timer<T>(T *instance, CallbackFunct funct, bool autoReset = false, unsigned int milliseconds = 0)
    {
        timerCBInstance(instance);
        timerCallback(funct);
        time_ms(milliseconds);
        AutoReset(autoReset);
    }

    ~Timer() {}

    void setCallback(CallbackFunct cb)
    {
        assert(cb != NULL);
        if (cb != NULL)
            timerCallback(cb);
    }

    bool Start()
    {
        if (timerActive())  // the timer is currently active
        {
            return false; // a timer is currently timing, return false
        }

        createTimer();  //no timer is currently active, 
        return true;
    }

    void Stop()
    {
        if (!timerActive())
            return; //timer is already stopped
        /* setup nanosleep */
        struct timespec req;
        req.tv_sec = 0;
        req.tv_nsec = (long)threadDeathAndTimeout_CheckTime / 4L * 1000L; //setup sleep to be threadDeathCheckTime / 4  microseconds
        /* END setup nanosleep */

        killTimer(true); //send kill timer command to timer thread

        while (killTimer())  //wait for timer thread to die
        {
            /** nanosleep for threadDeathAndTimeout_CheckTime/4 
             * microseconds after every check for 
            * timer death. The timer thread checks for a kill
            * command every threadDeathAndTimeout_CheckTime 
            * microseconds. Every 
            * threadDeathAndTimeout_CheckTime / 4 microseconds 
            * we will check to see if timer thread has died.   */
            nanosleep(&req, (struct timespec *)NULL);
        }
    }

    void setTime(unsigned int milliseconds)
    {
        time_ms(milliseconds);
    }

    unsigned int getTime()
    {
        return time_ms();
    }

    bool AutoReset()
    {
        bool temp;

        timerAutoControl.lock();
        temp = _AutoReset;
        timerAutoControl.unlock();

        return temp;
    }

    void AutoReset(bool value)
    {
        timerAutoControl.lock();
        _AutoReset = value;
        timerAutoControl.unlock();
    }

    bool isActive()
    {
        return timerActive();
    }

  private:
    bool _restartTimer = false;
    bool _AutoReset = false;
    bool _timerActive = false;
    bool _killTimer = false;
    unsigned int _time_ms = 0;

    CallbackFunct tcb = NULL; //the callback function
    T *_instance = NULL;      //the instance of the class containing the callback function

    /* mutexes for thread access control */
    std::mutex timerIgnoreControl;
    std::mutex timerActiveControl;
    std::mutex timerAutoControl;
    std::mutex timerTimeControl;
    std::mutex timerRestartControl;
    std::mutex timerCBInstanceControl;
    std::mutex timerCallbackControl;

    T *timerCBInstance()
    {
        T *temp;

        timerCBInstanceControl.lock();
        temp = _instance;
        timerCBInstanceControl.unlock();

        return temp;
    }

    void timerCBInstance(T *value)
    {
        timerCBInstanceControl.lock();
        _instance = value;
        timerCBInstanceControl.unlock();
    }

    CallbackFunct timerCallback()
    {
        CallbackFunct temp;

        timerCallbackControl.lock();
        temp = tcb;
        timerCallbackControl.unlock();

        return temp;
    }

    void timerCallback(CallbackFunct value)
    {
        timerCallbackControl.lock();
        tcb = value;
        timerCallbackControl.unlock();
    }

    bool timerActive()
    {
        bool temp;

        timerActiveControl.lock();
        temp = _timerActive;
        timerActiveControl.unlock();

        return temp;
    }

    void timerActive(bool value)
    {
        timerActiveControl.lock();
        _timerActive = value;
        timerActiveControl.unlock();
    }

    bool killTimer()
    {
        bool temp;

        timerIgnoreControl.lock();
        temp = _killTimer;
        timerIgnoreControl.unlock();

        return temp;
    }

    void killTimer(bool value)
    {
        timerIgnoreControl.lock();
        _killTimer = value;
        timerIgnoreControl.unlock();
    }

    bool restartTimer()
    {
        bool temp;

        timerRestartControl.lock();
        temp = _restartTimer;
        timerRestartControl.unlock();

        return temp;
    }

    void restartTimer(bool value)
    {
        timerRestartControl.lock();
        _restartTimer = value;
        timerRestartControl.unlock();
    }

    unsigned int time_ms()
    {
        unsigned int temp;

        timerTimeControl.lock();
        temp = _time_ms;
        timerTimeControl.unlock();

        return temp;
    }

    void time_ms(unsigned int value)
    {
        timerTimeControl.lock();
        _time_ms = value;
        timerTimeControl.unlock();
    }

    void createTimer()
    {
        assert(time_ms() > 0);
        assert(timerCallback() != NULL);
        assert(timerCBInstance() != NULL);

        timerActive(true);

        std::thread *myThread;

        myThread = new std::thread(&Timer::timerFunct, this);

        myThread->detach(); //start thread
    }

    void timerFunct()
    {
        unsigned int ms;
        CallbackFunct cb;
        struct timespec req;

#if 1 //this method should still be accurate and contains the ability for the thread to be killed every millisecond

        bool quit = false;
        std::chrono::_V2::system_clock::time_point startTime;
        req.tv_sec = 0;
        req.tv_nsec = (long)threadDeathAndTimeout_CheckTime * 1000L; //this controls the base sleep time of the timer loop. aka controls how often we check to see if we timed out or a kill command was sent

        do
        {
            startTime = sysClock::now(); //store the current system time for comparison later

            cb = timerCallback(); //save the callback ptr locally
            restartTimer(false);  //is there a need to restart the timer? No because if we're here we've been restarted
            ms = time_ms();       //update how long the timer is supposed to time for in milliseconds in case it has been changed

            do
            {
                nanosleep(&req, (struct timespec *)NULL); //nano sleep for 1 millisecond
                quit = killTimer();
            } while (!quit && (int64_t)1 <= ms - std::chrono::duration_cast<std::chrono::milliseconds>(sysClock::now() - startTime).count()); //loop until we have timed out or until we are told to quit

            if (!quit) //this is case if the timer has not been stoped by the Stop() function
            {
                (timerCBInstance()->*cb)();
            }

            //this will loop if the timer is being reset preventing the costly processing of destroying and re-creating a thread.
        } while ((AutoReset() || restartTimer()) && quit == false); //if AutoReset or restartTimer is true and killTimer is always false, loop. If killTimer is true, do not loop ever.

#else //very accurate timer. But no ability to interrupt timer if we want to stop the thread. But could provide ability to accurately time microseconds
        do
        {

            // startTime = sysClock::now();
            cb = timerCallback(); //save the callback ptr locally
            restartTimer(false);  //is there a need to restart the timer? No because if we're here we've been restarted
            ms = time_ms();       //update millisecond timeout time in case it has been changed

            req.tv_sec = ms / 1000;
            ms -= req.tv_sec * 1000;

            req.tv_nsec = ms * 1000000L;

            nanosleep(&req, (struct timespec *)NULL);
            if (killTimer())
                break;
            (timerCBInstance()->*cb)();

            //this will loop if the timer is being reset preventing the costly processing of destroying and re-creating a thread.
        } while ((AutoReset() || restartTimer()) && killTimer() == false); //if AutoReset or restartTimer is true and killTimer is always false, loop. If killTimer is true, do not loop ever.

#endif
        killTimer(false);
        timerActive(false);
    }
};

#endif
