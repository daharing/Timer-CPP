#include <stdio.h>
#include <mutex>
#include <unistd.h> //sleep

#include "timer.h"

using namespace std;

/** the TestClass is a class designed to both test and demonstrate
 * how to use the thread timer. You can inherit or instantiate 
 * the Timer class but you must inherit the TimerCallback
 * class. The TimerCallback class contains an abstract function 
 * that you must define and declare in your class for the timer
 * callback on timer timeout.
 * 
 * This class works by implementing a thread safe timerCallback function
 * (from the TimerCallback class) and instantiating an object of the 
 * Timer class. It gives examples of how to use the timer class. */
class TestClass : public TimerCallback
{
public:
  TestClass() { timer = new Timer(1000, this); }
  ~TestClass() {}

  void TimerTimeout()
  {

    accessControl.lock();
    bool isSelf = selfStartTimer;
    accessControl.unlock();

    if (isSelf)
    {
      /* this if statement that is changing what the timer's time
      is is optional */
      if (timer->getTime() == 2000)
      {
        timer->setTime(500);
        printf("I'm in the callback. Long timer time expired. Self reset with Short time\n");
      }
      else
      {
        timer->setTime(2000);
        printf("I'm in the callback. Short timer time expired. Self reset with long time\n");
      }

      timer->Start(); //Start the timer

    }
    else //if we are self starting the timer, come here
    {
      printf("I'm in the callback\n");
    }
  }

  void start()
  {

    printf("\nDemonstrate simple timer\n");
    timer->setTime(2000);
    timer->Start();
    sleep(5);

    printf("\nDemonstrate timer with auto reset\n");
    timer->AutoReset(true);
    timer->setTime(1000);
    timer->Start();

    sleep(6);
    timer->AutoReset(false);
    timer->Stop(); //after using stop, must still wait the remaining amount of time for timer timeout before calling start
    sleep(1);

    printf("\nDemonstrate timer with self reset in callback\n");
    timer->setTime(1000);
    selfStartTimer = true;
    timer->Start();

    sleep(10);

    timer->Stop();

    sleep(2);
  }

private:
  bool selfStartTimer = false;
  std::mutex accessControl;
  Timer *timer;
};



int main()
{
  TestClass test;
  test.start();

  return 0;
}
