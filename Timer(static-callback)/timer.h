/*
 * Author:  Dustin Haring
 * Date:    May 29, 2018
 * Comment: This file contains the declarations and definitions for the
 *          different thread timers available. One timer per instance.
 * 
 * Modified By: n/a
 * Date:        n/a
 * Comment:     n/a
 */
#ifndef ThreadTimer_H
#define ThreadTimer_H

// #include <stdio.h>
#include <thread>
#include <mutex>
#include <unistd.h> //sleep
#include <assert.h>

/** INSTRUCTIONS*****
 * Create an instance of the Timer class and call
 * setTimer to set a time in ms and Start to start the
 * timer.
 * 
 * */

/** inherit this class and implement the
 * callback function. This function is where
 * all the timer threads go when timed out. */
class TimerCallback
{
  public:
    virtual void TimerTimeout() = 0;
};



class Timer
{
  public:
    Timer(unsigned int milliseconds, TimerCallback *cb, bool autoReset = false)
    {
        time_ms(milliseconds);
        tcb = cb;
        AutoReset(autoReset);
    }
    ~Timer() {}

    bool Start()
    {
        if (timerActive())
        {
            if (ignoreTimer())
                return false;
            restartTimer(true);
            return true;
        }

        createTimer();
        return true;
    }

    void Stop()
    {
        ignoreTimer(true);
    }

    void setTime(unsigned int milliseconds)
    {
        time_ms(milliseconds);
    }

    unsigned int getTime()
    {
        return time_ms();
    }

    bool AutoReset()
    {
        bool temp;

        timerAutoControl.lock();
        temp = _AutoReset;
        timerAutoControl.unlock();

        return temp;
    }

    void AutoReset(bool value)
    {
        timerAutoControl.lock();
        _AutoReset = value;
        timerAutoControl.unlock();
    }


  private:
    bool _restartTimer = false;
    bool _AutoReset = false;
    bool _timerActive = false;
    bool _ignoreTimer = false;
    unsigned int _time_ms = 0;

    TimerCallback *tcb = NULL;

    std::mutex timerIgnoreControl;
    std::mutex timerActiveControl;
    std::mutex timerAutoControl;
    std::mutex timerTimeControl;
    std::mutex timerRestartControl;

    bool timerActive()
    {
        bool temp;

        timerActiveControl.lock();
        temp = _timerActive;
        timerActiveControl.unlock();

        return temp;
    }

    void timerActive(bool value)
    {
        timerActiveControl.lock();
        _timerActive = value;
        timerActiveControl.unlock();
    }

    bool ignoreTimer()
    {
        bool temp;

        timerIgnoreControl.lock();
        temp = _ignoreTimer;
        timerIgnoreControl.unlock();

        return temp;
    }

    void ignoreTimer(bool value)
    {
        timerIgnoreControl.lock();
        _ignoreTimer = value;
        timerIgnoreControl.unlock();
    }

    bool restartTimer()
    {
        bool temp;

        timerRestartControl.lock();
        temp = _restartTimer;
        timerRestartControl.unlock();

        return temp;
    }

    void restartTimer(bool value)
    {
        timerRestartControl.lock();
        _restartTimer = value;
        timerRestartControl.unlock();
    }

    unsigned int time_ms()
    {
        unsigned int temp;

        timerTimeControl.lock();
        temp = _time_ms;
        timerTimeControl.unlock();

        return temp;
    }

    void time_ms(unsigned int value)
    {
        timerTimeControl.lock();
        _time_ms = value;
        timerTimeControl.unlock();
    }

    void createTimer()
    {
        assert(time_ms() > 0);
        assert(tcb != NULL);

        timerActive(true);

        std::thread *myThread;

        myThread = new std::thread(&Timer::timerFunct, this);

        myThread->detach(); //start thread
    }

    void timerFunct()
    {
        unsigned int ms;
        do
        {
            restartTimer(false);
            ms = time_ms();
            if (ms < 1000) //if less than one second of time
            {
                do
                {
                    usleep(ms * 1000);
                    if (ignoreTimer())
                        break;
                    tcb->TimerTimeout();
                } while (AutoReset());
            }
            else //else >= 1 seconds of time
            {
                do
                {
                    sleep(ms / 1000);
                    if (ignoreTimer())
                        break;
                    tcb->TimerTimeout();
                } while (AutoReset());
            }
        }while (restartTimer() && ignoreTimer() == false);
        
        ignoreTimer(false);
        timerActive(false);
    }
};

#endif
